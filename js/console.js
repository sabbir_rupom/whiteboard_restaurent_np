$(function () {
    $(document).on('click', '.api-nav', function () {
        var post = $(this).data('post');
        var api = $(this).data('api');
        var user_required = parseInt($(this).data('user_req'));

        if (user_required > 0) {
            $("input[name=user_id]").attr('disabled', false);
        } else {
            $("input[name=user_id]").attr('disabled', true).val('');
        }

        var result = post.split(',');
        $("input[name=url]").val(api);
        if (result.length > 0) {
            if (result[0] != '') {
                var body = '{';
                result.forEach(function (value, index) {
                    if (index > 0) {
                        body += ',';
                    }
                    body += '"' + value + '":""';
//                    body .= '';
                });
                body += '}';
                $("textarea[name=requestBody]").val(body);
            } else {
                $("textarea[name=requestBody]").val('');
            }
        } else {
            $("textarea[name=requestBody]").val('');
        }
        $('#submit').removeAttr('disabled');
    });
});

function callApi() {
    $('#json_output').html('');
    var api_name = $("input[name=url]").val();
    var json_body = $("textarea[name=requestBody]").val();
    var check_json = json_body != '' ? isJson(json_body) : true;
    var user_id = parseInt($("input[name=user_id]").val());
    var session_token = $("input[name=session_token]").val();

    var header = {
        "alg": "HS256",
        "typ": "JWT"
    };

    var data = {
        "sessionToken": session_token,
        "userID": user_id
    };

    var secret_key = $("input[name=token_key]").val();

    var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));

    var encodedHeader = base64url(stringifiedHeader);
//document.getElementById("header").innerText = encodedHeader;

    var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
    var encodedData = base64url(stringifiedData);
//document.getElementById("payload").innerText = encodedData;

    var signature = encodedHeader + "." + encodedData;
    signature = CryptoJS.HmacSHA256(signature, secret_key);
    signature = base64url(signature);

    var signature_token = encodedHeader + '.' + encodedData + '.' + signature;
    $('#hash_signature').val(encodedHeader + '.' + encodedData + '.' + signature);

    if (check_json) {
        $.ajax({
            url: base_url + api_name,
            type: 'POST',
            data: json_body,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-QUIZ-TOKEN', signature_token);
            },
            success: function (obj) {
                if (obj.hasOwnProperty('sessionToken')) {
                    $("input[name=session_token]").val(obj.sessionToken);
                    $("input[name=user_id]").val(obj.userId);
                }
                if (obj.hasOwnProperty('success')) {
                    $("#json_output").closest("pre").css("background-color", "#dff0d8");
                } else {
                    $("#json_output").closest("pre").css("background-color", "rgb(251, 205, 183)");
                }
                var str = JSON.stringify(obj, null, 2);
                $('#json_output').text(syntaxHighlight(str));

            }
        }).fail(function (obj) {
            var str = JSON.stringify(obj, null, 2);
            $("#json_output").closest("pre").css("background-color", "rgb(251, 205, 183)");
            $('#json_output').text(syntaxHighlight(str));
        });
    } else {
        alert('invalid JSON');
    }
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    return json;
//    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
//    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
//        var cls = 'number';
//        if (/^"/.test(match)) {
//            if (/:$/.test(match)) {
//                cls = 'key';
//            } else {
//                cls = 'string';
//            }
//        } else if (/true|false/.test(match)) {
//            cls = 'boolean';
//        } else if (/null/.test(match)) {
//            cls = 'null';
//        }
//        return '<span class="' + cls + '">' + match + '</span>';
//    });
}

function base64url(source) {
    // Encode in classical base64
    var encodedSource = CryptoJS.enc.Base64.stringify(source);

    // Remove padding equal characters
    encodedSource = encodedSource.replace(/=+$/, '');

    // Replace characters according to base64url specifications
    encodedSource = encodedSource.replace(/\+/g, '-');
    encodedSource = encodedSource.replace(/\//g, '_');

    return encodedSource;
}