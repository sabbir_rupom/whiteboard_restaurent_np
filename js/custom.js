$(document).ready(function () {
    // Question Iamge View when upload
    image = 'image';
    image_view = 'image-view';
    imageView(image, image_view);

    $('#user_add').submit(function () {
        return check_unique_email();
    });
    $("#user_add").validate();
});
function imageView(inputFileName, viewClassName) {
    $("input[name=" + inputFileName + "]").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $('<img>').attr('src', e.target.result);
                $('.' + viewClassName).html(img);
            };
            reader.readAsDataURL(this.files[0]);
        }
        $(".question-img").show();
    });
}
function showPassword(obj) {
    var key_attr = $('#password').attr('type');
    if (key_attr !== 'text') {
        $(obj).removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
        $('#password').attr('type', 'text');
    } else {
        $(obj).removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
        $('#password').attr('type', 'password');
    }
}
function check_unique_email() {
    var email = $("#email").val();
    $.ajax({
        type: "POST",
        url: base_url + 'admin/check_unique_email',
        data: 'email=' + email,
        success: function (data) {
            if (data == 1) {
                $("#unique_email_req").text("");
                $("#email").css('border-color', '#d2d6de ');
                return true;
            }
            if (data == 2) {
                $("#unique_email_req").text("This email is already registered with us, please try again.").css('color', '#dd4b39 ');
                $("#email").css('border-color', '#dd4b39 ');
                return false;
            }
        }
    });
}
function check_unique_user() {
    var email = $("#user_name").val();
    $.ajax({
        type: "POST",
        url: base_url + 'admin/check_unique_user',
        data: 'user_name=' + email,
        success: function (data) {
            if (data == 1) {
                $("#unique_user_req").text("");
                $("#email").css('border-color', '#d2d6de ');
                return true;
            }
            if (data == 2) {
                $("#unique_user_req").text("This user name is already registered with us, please try again.").css('color', '#dd4b39 ');
                $("#email").css('border-color', '#dd4b39 ');
                return false;
            }
        }
    });
}