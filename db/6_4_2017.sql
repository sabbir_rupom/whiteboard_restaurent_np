-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for restaurant_whiteboard
CREATE DATABASE IF NOT EXISTS `restaurant_whiteboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `restaurant_whiteboard`;

-- Dumping structure for table restaurant_whiteboard.admin_user
CREATE TABLE IF NOT EXISTS `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` int(1) NOT NULL COMMENT 'Table User Role',
  `user_token` varchar(30) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- Dumping data for table restaurant_whiteboard.admin_user: ~55 rows (approximately)
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` (`id`, `full_name`, `email`, `username`, `password`, `role`, `user_token`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '', 1, '2016-05-20 12:26:09', '2018-04-13 10:42:23'),
	(6, 'Data Entry User', 'mustafa@a.com', 'mustafa', '4dcf3d23e27d63919ad8f4fda123cdd7', 2, '', 0, '2017-07-14 17:55:25', '2017-07-14 17:55:25'),
	(7, 'mustafa Ozturk', 'mustafa.ozturk@yerliarge.com', 'mustafa_o', 'ec610efecc0d19bdee2c1052e0af88a4', 2, '', 0, '2017-07-31 11:26:06', '2017-07-31 08:26:06'),
	(9, 'cc cc', 'cc@email.com', 'cc', 'e0323a9039add2978bf5b49550572c7c', 2, '', 0, '2018-02-16 14:11:02', '2018-02-16 19:11:02'),
	(10, 'cc cc', 'cc@email.com', 'cc', 'e0323a9039add2978bf5b49550572c7c', 2, '', 0, '2018-02-16 14:12:03', '2018-02-16 19:12:03'),
	(12, 'Test Test', 't@e.net', 'Test', '6f8f57715090da2632453988d9a1501b', 2, '', 0, '2018-02-16 14:15:35', '2018-02-16 19:15:35'),
	(13, 'Test Test', 't@e.net', 'Test', '6f8f57715090da2632453988d9a1501b', 2, '', 0, '2018-02-16 14:19:13', '2018-02-16 19:19:13'),
	(14, 'Test Test', 't@e.net', 'Test', '6f8f57715090da2632453988d9a1501b', 2, '', 0, '2018-02-16 14:20:39', '2018-02-16 19:20:39'),
	(15, 'Emrul C', 'res1@email.com', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 08:13:08', '2018-03-01 13:13:08'),
	(16, 'Emrul C', 'res2@email.com', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:27:39', '2018-03-01 15:27:39'),
	(17, 'Emrul C', 'res2@email.com', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:36:17', '2018-03-01 15:36:17'),
	(18, 'Emrul C', 'cd@w.net', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:37:01', '2018-03-01 15:37:01'),
	(19, 'Emrul C', 'cd@w.net', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:39:23', '2018-03-01 15:39:23'),
	(20, 'Emrul C', 'cd@w.net', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:43:41', '2018-03-01 15:43:41'),
	(21, 'Emrul C', 'cd@w.net', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:45:51', '2018-03-01 15:45:52'),
	(22, 'Emrul C', 'cd@w.net', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:46:35', '2018-03-01 15:46:36'),
	(23, 'Emrul C', 'cd@w.net', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:47:43', '2018-03-01 15:47:43'),
	(24, 'Emrul C', 'cd@w.net', 'Emrul', '202cb962ac59075b964b07152d234b70', 2, '', 0, '2018-03-01 10:48:26', '2018-03-01 15:48:26'),
	(25, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 10:53:50', '2018-03-01 15:53:51'),
	(26, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 10:55:19', '2018-03-01 15:55:19'),
	(27, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:00:27', '2018-03-01 16:00:27'),
	(28, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:02:01', '2018-03-01 16:02:02'),
	(29, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:03:31', '2018-03-01 16:03:31'),
	(30, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:07:31', '2018-03-01 16:07:31'),
	(31, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:08:56', '2018-03-01 16:08:56'),
	(32, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:09:20', '2018-03-01 16:09:20'),
	(33, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:09:48', '2018-03-01 16:09:48'),
	(34, 'bb cc', 'cc@s.com', 'bb', 'c4ca4238a0b923820dcc509a6f75849b', 2, '', 0, '2018-03-01 11:28:19', '2018-03-01 16:28:19'),
	(36, 'res_admin res_admin', 'res_admin@gmail.com', 'res_admin', '9b207167e5381c47682c6b4f58a623fb', 2, '', 0, '2018-04-03 09:26:09', '2018-04-03 15:04:42'),
	(37, 'testsil testsil', 'testsil@gmail.com', 'testsil', 'ae59acd784a775e2c31f3b1c4db44f4f', 2, '', 0, '2018-04-03 09:27:51', '2018-04-03 13:27:51'),
	(38, 'res_test', 'res_test@gmail.com', 'res_test', '9eebbcfe0d88a88687d5356e459a25e3', 2, '', 0, '2018-04-03 09:36:36', '2018-04-03 13:36:36'),
	(39, 'test test', 'test@gmail.com', 'test', 'cc03e747a6afbbcbf8be7668acfebee5', 2, '', 0, '2018-04-03 10:03:40', '2018-04-03 14:03:40'),
	(40, 'res_newuser', 'res_newuser@gmail.com', 'res_newuser', '4dff90be4d5fbd56858d4eed43c25300', 1, '', 0, '2018-04-03 10:14:06', '2018-04-03 14:14:06'),
	(41, 's s', 'ss@gmail.com', 's', '3691308f2a4c2f6983f2880d32e29c84', 2, '', 0, '2018-04-03 12:23:33', '2018-04-03 16:23:33'),
	(42, 'res res', 'res@gmail.com', 'res', '9b207167e5381c47682c6b4f58a623fb', 2, '', 0, '2018-04-03 12:25:20', '2018-04-03 16:25:20'),
	(45, 'test', 'testtest@gmail.com', 'testuser', '4266bf8d3dc65bc84fd3badf2edfdbe7', 1, '', 0, '2018-04-04 12:52:32', '2018-04-04 16:52:32'),
	(46, 'newuser', 'newuser@gmail.com', 'newuser', '', 3, '', 0, '0000-00-00 00:00:00', '2018-04-04 16:58:02'),
	(47, 'abc abc', 'abc@gmail.com', 'abc', '440ac85892ca43ad26d44c7ad9d47d3e', 2, '', 0, '2018-04-05 07:57:14', '2018-04-11 15:24:00'),
	(48, 'res_new res_new', 'res_new@gmail.com', 'res_new', '505eebc4b306509c3b9936970724c08a', 2, '', 0, '2018-04-05 08:01:34', '2018-04-05 12:01:34'),
	(49, 'res_owner res_owner', 'res_owner@gmail.com', 'res_owner', 'b7095ab04e08dc8ff0f6aee848fa017d', 2, '', 0, '2018-04-05 08:04:43', '2018-04-05 12:04:44'),
	(51, 'local local', 'local@gmail.com', 'local', 'f5ddaf0ca7929578b408c909429f68f2', 2, '', 0, '2018-04-05 08:16:42', '2018-04-05 12:16:42'),
	(52, 'sample sample', 'sample@gmail.com', 'sample', '5e8ff9bf55ba3508199d22e984129be6', 2, '', 0, '2018-04-05 08:29:57', '2018-04-05 12:29:57'),
	(53, 'sam sam', 'sam@gmail.com', 'sam', 'eadd934e2cc978fc622fc1324878d8af', 2, '', 0, '2018-04-05 08:32:21', '2018-04-05 12:32:21'),
	(54, 'new_sol new_sol', 'new_sol@gmail.com', 'new_sol', '4ee2d24afadb64d2fa7031191b77ea76', 2, '', 0, '2018-04-05 08:52:26', '2018-04-05 12:52:26'),
	(55, 'new_sol new_sol', 'new_sol@email.com', 'new_sol', '4ee2d24afadb64d2fa7031191b77ea76', 2, '', 0, '2018-04-05 11:05:59', '2018-04-05 15:05:59'),
	(57, 'kalam kalam', 'kalam@email.com', 'kalam', '91b05dea6f3ba6c9048a0f6f6f33620e', 2, '', 0, '2018-04-05 11:09:56', '2018-04-05 15:09:56'),
	(58, 'kalam kalam', 'kalam@em.com', 'kalam', '91b05dea6f3ba6c9048a0f6f6f33620e', 2, '', 0, '2018-04-05 11:12:22', '2018-04-05 15:12:22'),
	(59, 'owner owner', 'owner@gmail.com', 'owner', '72122ce96bfec66e2396d2e25225d70a', 2, '', 0, '2018-04-05 11:34:17', '2018-04-05 15:34:17'),
	(60, 'user_token user_token', 'user_token@gmail.com', 'user_token', '6ff16d3b2957297735fbf4299a3c8c64', 2, '885', 1, '2018-04-06 10:45:59', '2018-04-12 12:42:03'),
	(61, 'def def', 'def@gmail.com', 'def', '77bd7ea3b0a76184dde27211c641f70d', 2, '877', 0, '2018-04-06 12:19:25', '2018-04-06 16:19:25'),
	(62, 'local local', 'locallocal@gmail.com', 'local', 'f5ddaf0ca7929578b408c909429f68f2', 2, '400', 0, '2018-04-09 10:39:38', '2018-04-09 14:39:38'),
	(63, 'locallll locall', 'local@email.com', 'locallll', 'f5ddaf0ca7929578b408c909429f68f2', 2, '170', 0, '2018-04-09 10:50:06', '2018-04-09 14:50:06'),
	(64, 'testlocal testlocal', 'testlocal@gmail.com', 'testlocal', '43ec5c62d5541cc0e0b4ca3a7b532926', 2, '286', 0, '2018-04-09 10:51:40', '2018-04-09 14:51:40'),
	(66, 'slick slick', 'slick@gmail.com', 'slick', 'b91610852980a3995a03d0ecd1bb5143', 2, '0353ab4cbed5beae847a7ff6e220b5', 0, '2018-04-11 13:49:20', '2018-04-11 17:49:20'),
	(89, 'azad_res azad_res', 'azadsol30@gmail.com', 'azad_res', 'f9819e4d963f46cbc169f56bea1f2cc7', 2, 'f718499c1c8cef6730f9fd03c8125c', 1, '2018-04-23 13:08:19', '2018-04-23 17:09:55');
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.app_users
CREATE TABLE IF NOT EXISTS `app_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `deviceudid` varchar(100) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `deviceudid` (`deviceudid`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- Dumping data for table restaurant_whiteboard.app_users: ~40 rows (approximately)
/*!40000 ALTER TABLE `app_users` DISABLE KEYS */;
INSERT INTO `app_users` (`id`, `username`, `deviceudid`, `full_name`, `email`, `status`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'test1123', NULL, NULL, 1, '2017-07-27 07:58:44', '2017-07-27 04:58:44'),
	(2, NULL, '1111', NULL, NULL, 1, '2017-08-14 13:32:50', '2017-08-14 13:32:50'),
	(3, NULL, 'qqqqqq', NULL, NULL, 1, '2017-08-16 14:46:41', '2017-08-16 14:46:41'),
	(4, NULL, '123', NULL, NULL, 1, '2018-01-23 10:55:23', '2018-01-23 15:55:23'),
	(5, 'appusers1', '111', NULL, NULL, 1, '2018-01-24 06:23:50', '2018-04-09 12:31:17'),
	(6, 'appusers2', '222', NULL, NULL, 1, '2018-01-30 07:42:16', '2018-04-09 12:31:56'),
	(7, NULL, '1123', NULL, NULL, 1, '2018-04-10 11:31:15', '2018-04-10 15:31:15'),
	(8, NULL, '4wd3ed', NULL, NULL, 1, '2018-04-10 11:31:26', '2018-04-10 15:31:26'),
	(9, NULL, 'SDdSdS', NULL, NULL, 1, '2018-04-10 11:31:41', '2018-04-10 15:31:41'),
	(10, NULL, 'FDLFJDKLFADL', NULL, NULL, 1, '2018-04-10 11:32:46', '2018-04-10 15:32:46'),
	(11, NULL, '12', NULL, NULL, 1, '2018-04-10 11:32:51', '2018-04-10 15:32:51'),
	(12, NULL, '34', NULL, NULL, 1, '2018-04-10 11:32:54', '2018-04-10 15:32:54'),
	(13, NULL, '123456789', NULL, NULL, 1, '2018-04-10 11:33:03', '2018-04-10 15:33:03'),
	(14, NULL, '345', NULL, NULL, 1, '2018-04-10 12:02:12', '2018-04-10 16:02:12'),
	(15, NULL, '234567', NULL, NULL, 1, '2018-04-10 12:17:34', '2018-04-10 16:17:34'),
	(16, NULL, '1234', NULL, NULL, 1, '2018-04-10 12:37:33', '2018-04-10 16:37:33'),
	(17, NULL, '555555', NULL, NULL, 1, '2018-04-10 13:50:39', '2018-04-10 17:50:39'),
	(18, NULL, '2w3', NULL, NULL, 1, '2018-04-10 13:51:06', '2018-04-10 17:51:06'),
	(19, NULL, '6789', NULL, NULL, 1, '2018-04-10 13:56:39', '2018-04-10 17:56:39'),
	(20, NULL, '23445', NULL, NULL, 1, '2018-04-11 05:59:01', '2018-04-11 09:59:01'),
	(21, NULL, '234', NULL, NULL, 1, '2018-04-11 06:38:10', '2018-04-11 10:38:10'),
	(22, NULL, '33', NULL, NULL, 1, '2018-04-11 06:39:51', '2018-04-11 10:39:51'),
	(23, NULL, '3', NULL, NULL, 1, '2018-04-11 06:40:12', '2018-04-11 10:40:12'),
	(24, NULL, 'hju77', NULL, NULL, 1, '2018-04-11 07:22:49', '2018-04-11 11:22:49'),
	(25, NULL, 'w', NULL, NULL, 1, '2018-04-11 07:25:03', '2018-04-11 11:25:03'),
	(26, NULL, '23', NULL, NULL, 1, '2018-04-11 07:28:46', '2018-04-11 11:28:46'),
	(27, NULL, '333', NULL, NULL, 1, '2018-04-11 08:04:38', '2018-04-11 12:04:38'),
	(28, NULL, '44', NULL, NULL, 1, '2018-04-11 08:44:24', '2018-04-11 12:44:24'),
	(29, NULL, '4', NULL, NULL, 1, '2018-04-23 08:38:53', '2018-04-23 12:38:53'),
	(30, NULL, '444', NULL, NULL, 1, '2018-04-23 08:38:57', '2018-04-23 12:38:58'),
	(31, NULL, '00', NULL, NULL, 1, '2018-04-24 05:39:19', '2018-04-24 09:39:19'),
	(32, NULL, '6', NULL, NULL, 1, '2018-04-24 06:00:58', '2018-04-24 10:00:58'),
	(33, NULL, '555', NULL, NULL, 1, '2018-04-24 06:13:19', '2018-04-24 10:13:19'),
	(34, NULL, '100', NULL, NULL, 1, '2018-04-24 06:13:59', '2018-04-24 10:13:59'),
	(35, NULL, '54', NULL, NULL, 1, '2018-04-24 06:31:17', '2018-04-24 10:31:17'),
	(36, NULL, '5', NULL, NULL, 1, '2018-04-24 07:08:13', '2018-04-24 11:08:13'),
	(37, NULL, '8', NULL, NULL, 1, '2018-04-24 07:21:54', '2018-04-24 11:21:54'),
	(38, NULL, '11', NULL, NULL, 1, '2018-04-24 12:54:31', '2018-04-24 16:54:31'),
	(39, NULL, '11111', NULL, NULL, 1, '2018-04-24 12:54:39', '2018-04-24 16:54:39'),
	(40, NULL, '111111', NULL, NULL, 1, '2018-04-24 12:54:42', '2018-04-24 16:54:42');
/*!40000 ALTER TABLE `app_users` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.authsessions
CREATE TABLE IF NOT EXISTS `authsessions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `tokenid` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table restaurant_whiteboard.authsessions: ~40 rows (approximately)
/*!40000 ALTER TABLE `authsessions` DISABLE KEYS */;
INSERT INTO `authsessions` (`id`, `userid`, `tokenid`, `created_at`, `updated_at`) VALUES
	(1, 9, 'MTUyMzM1MjcwMV82MDI4', '2017-08-01 13:15:51', '2018-04-10 15:31:41'),
	(2, 1, 'MTUwMjM2MjQ1NF84MzQz', '2017-08-10 12:54:14', '2017-08-10 16:54:14'),
	(3, 2, 'MTUyNDU2NzI3Nl8zNzY5', '2017-08-14 13:32:50', '2018-04-24 16:54:36'),
	(4, 3, 'MTUwMjg3MzIwMV83ODk2', '2017-08-16 14:46:41', '2017-08-16 14:46:41'),
	(5, 4, 'MTUyMzQyNDIwOV82NTQ3', '2018-01-23 10:55:23', '2018-04-11 11:23:29'),
	(6, 5, 'MTUyNDU2NzI3NF85Nzg2', '2018-01-24 06:23:50', '2018-04-24 16:54:34'),
	(7, 6, 'MTUyMzQyMTU2NV83MzU5', '2018-01-30 07:42:16', '2018-04-11 10:39:25'),
	(8, 7, 'MTUyMzM1MjY3NV8zMzg1', '2018-04-10 11:31:15', '2018-04-10 15:31:15'),
	(9, 8, 'MTUyMzM1MjY4Nl85MjI1', '2018-04-10 11:31:26', '2018-04-10 15:31:26'),
	(10, 10, 'MTUyMzM1Mjc2Nl84NzM3', '2018-04-10 11:32:46', '2018-04-10 15:32:46'),
	(11, 11, 'MTUyMzQyNTg5NF8yOTUz', '2018-04-10 11:32:51', '2018-04-11 11:51:34'),
	(12, 12, 'MTUyMzM1Mjc3NF83MTE3', '2018-04-10 11:32:54', '2018-04-10 15:32:54'),
	(13, 13, 'MTUyMzM1Mjc5NF83NzQz', '2018-04-10 11:33:03', '2018-04-10 15:33:14'),
	(14, 14, 'MTUyMzM1NDYwMF8xODUz', '2018-04-10 12:02:12', '2018-04-10 16:03:20'),
	(15, 15, 'MTUyMzM1NTQ1NF82ODU2', '2018-04-10 12:17:34', '2018-04-10 16:17:34'),
	(16, 16, 'MTUyMzM2MDk1M184ODk1', '2018-04-10 12:37:33', '2018-04-10 17:49:13'),
	(17, 17, 'MTUyMzM2MTAzOV81MzEw', '2018-04-10 13:50:39', '2018-04-10 17:50:39'),
	(18, 18, 'MTUyMzM2MTA3Nl8zNzc2', '2018-04-10 13:51:06', '2018-04-10 17:51:16'),
	(19, 19, 'MTUyMzM2MTM5OV82MDYz', '2018-04-10 13:56:39', '2018-04-10 17:56:39'),
	(20, 20, 'MTUyMzQxOTE0MV84NzQ4', '2018-04-11 05:59:01', '2018-04-11 09:59:01'),
	(21, 21, 'MTUyMzQyMTQ5MF8zMDk3', '2018-04-11 06:38:10', '2018-04-11 10:38:10'),
	(22, 22, 'MTUyMzQyMTU5MV84Nzg3', '2018-04-11 06:39:51', '2018-04-11 10:39:51'),
	(23, 23, 'MTUyMzQyMTYxMl81NzI4', '2018-04-11 06:40:12', '2018-04-11 10:40:12'),
	(24, 24, 'MTUyMzQyNDE2OV85ODQ5', '2018-04-11 07:22:49', '2018-04-11 11:22:49'),
	(25, 25, 'MTUyMzQyNDMwM18xOTY1', '2018-04-11 07:25:03', '2018-04-11 11:25:03'),
	(26, 26, 'MTUyMzQyOTg2MF85MTgy', '2018-04-11 07:28:46', '2018-04-11 12:57:40'),
	(27, 27, 'MTUyMzQyNjY3OF8zNjY3', '2018-04-11 08:04:38', '2018-04-11 12:04:38'),
	(28, 28, 'MTUyNDQ2NTU1Ml84MjM1', '2018-04-11 08:44:24', '2018-04-23 12:39:12'),
	(29, 29, 'MTUyNDU0MTAwMV81Nzk5', '2018-04-23 08:38:53', '2018-04-24 09:36:41'),
	(30, 30, 'MTUyNDQ2NTU0NV8zNDkx', '2018-04-23 08:38:57', '2018-04-23 12:39:05'),
	(31, 31, 'MTUyNDU0MTE1OV81NzY4', '2018-04-24 05:39:19', '2018-04-24 09:39:19'),
	(32, 32, 'MTUyNDU0MzIyMl82MzQ3', '2018-04-24 06:00:58', '2018-04-24 10:13:42'),
	(33, 33, 'MTUyNDU2Nzg0M184OTg1', '2018-04-24 06:13:19', '2018-04-24 17:04:03'),
	(34, 34, 'MTUyNDU0MzI1OV8yODE1', '2018-04-24 06:13:59', '2018-04-24 10:14:19'),
	(35, 35, 'MTUyNDU0NDI3N185NTEy', '2018-04-24 06:31:17', '2018-04-24 10:31:17'),
	(36, 36, 'MTUyNDU0NjQ5M18yOTcx', '2018-04-24 07:08:13', '2018-04-24 11:08:13'),
	(37, 37, 'MTUyNDU0NzMxNF83NjEz', '2018-04-24 07:21:54', '2018-04-24 11:21:54'),
	(38, 38, 'MTUyNDU2NzI3MV80NDQy', '2018-04-24 12:54:31', '2018-04-24 16:54:31'),
	(39, 39, 'MTUyNDU2NzI3OV84NzIw', '2018-04-24 12:54:39', '2018-04-24 16:54:39'),
	(40, 40, 'MTUyNDU2NzI4Ml84MjA3', '2018-04-24 12:54:42', '2018-04-24 16:54:42');
/*!40000 ALTER TABLE `authsessions` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.cart_tbl
CREATE TABLE IF NOT EXISTS `cart_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `unit` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- Dumping data for table restaurant_whiteboard.cart_tbl: ~3 rows (approximately)
/*!40000 ALTER TABLE `cart_tbl` DISABLE KEYS */;
INSERT INTO `cart_tbl` (`id`, `restaurant_id`, `user_id`, `menu_id`, `unit`, `price`, `created_at`) VALUES
	(48, 14, 5, 45, 5, 11110, '2018-04-26 12:41:17'),
	(49, 14, 5, 45, 5, 11110, '2018-04-26 12:41:21'),
	(55, 14, 5, 45, 5, 11110, '2018-04-26 12:41:25');
/*!40000 ALTER TABLE `cart_tbl` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table restaurant_whiteboard.categories: ~8 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(3, 'TEOG Yabancı Dil İngilizce', '2017-07-31 11:28:06', '2017-07-31 08:28:06'),
	(4, 'TEOG Yabancı Dil Almanca', '2017-08-01 14:59:28', '2017-08-01 11:59:28'),
	(5, 'TEOG Matematik', '2017-08-02 07:21:29', '2017-08-02 07:21:29'),
	(6, 'TEOG Türkçe', '2017-08-02 13:06:26', '2017-08-02 13:06:26'),
	(7, 'TEOG Tarih', '2017-08-04 11:59:16', '2017-08-04 11:59:16'),
	(8, 'TEOG Din Kültürü ve Ahlak Bilgisi', '2017-08-05 07:21:09', '2017-08-05 07:21:09'),
	(9, 'TEOG Fen Bilimleri', '2017-08-07 07:28:07', '2017-08-07 07:28:07'),
	(10, 'category2', '2018-04-03 10:08:21', '2018-04-03 14:08:21');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `unit_availability` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table restaurant_whiteboard.menu: ~8 rows (approximately)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `restaurant_id`, `name`, `price`, `description`, `unit_availability`, `created_at`, `updated_at`) VALUES
	(1, 2, 'menu2_1', '200', '222', 100, '2018-02-27 06:48:24', '2018-04-25 19:12:10'),
	(11, 89, 'Test', '120', '<p>\r\n	120 dollar menu edit</p>\r\n', 200, '2018-02-27 12:08:14', '2018-05-15 12:44:36'),
	(13, 89, 'test', '444', '<p>\r\n	affd</p>\r\n', 300, '2018-04-13 14:31:23', '2018-05-15 12:43:26'),
	(16, 5, '', '7890', '<p>\r\n	jk gr htvj grv</p>\r\n', 40, '2018-04-13 14:42:11', '2018-04-25 19:12:24'),
	(22, 3, 'test_menu', '4566', '<p>\r\n	test menu description</p>\r\n', 60, '2018-04-13 15:24:41', '2018-04-25 19:12:28'),
	(42, 13, 'test455', '44', 'dfd', 120, '2018-04-23 11:04:35', '2018-04-25 19:12:32'),
	(43, 13, 'menu2', '344', 'menu describes', 190, '2018-04-23 11:04:42', '2018-04-25 19:12:39'),
	(45, 13, 'testing', '2222', 'test no description', 300, '2018-04-24 10:50:40', '2018-04-25 19:12:43');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.orderdetails
CREATE TABLE IF NOT EXISTS `orderdetails` (
  `orderNumber` int(11) NOT NULL,
  `productCode` varchar(15) NOT NULL,
  `quantityOrdered` int(11) NOT NULL,
  `priceEach` double NOT NULL,
  `orderLineNumber` smallint(6) NOT NULL,
  PRIMARY KEY (`orderNumber`,`productCode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table restaurant_whiteboard.orderdetails: 0 rows
/*!40000 ALTER TABLE `orderdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderdetails` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `restaurant_id` int(11) NOT NULL DEFAULT '0',
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `unit` int(11) NOT NULL DEFAULT '0',
  `total_price` int(11) NOT NULL DEFAULT '0',
  `tokenid` varchar(50) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table restaurant_whiteboard.orders: ~25 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user_id`, `restaurant_id`, `menu_id`, `unit`, `total_price`, `tokenid`, `created_at`, `updated_at`) VALUES
	(5, 5, 89, 11, 1, 100, 'MTUxNjk3MTgyMl84MTU0', '2018-01-26 14:23:53', '2018-05-09 12:42:55'),
	(6, 6, 89, 11, 3, 300, '0', '0000-00-00 00:00:00', '2018-05-09 12:42:59'),
	(16, 6, 89, 11, 2, 44, '0', '2018-01-26 13:54:26', '2018-05-09 12:43:03'),
	(17, 0, 89, 2, 2, 44, 'MTUxNjk2NDg4NV8yNDMy', '0000-00-00 00:00:00', '2018-05-15 10:54:30'),
	(66, 5, 89, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-05-09 12:45:17'),
	(67, 5, 89, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-05-09 12:45:21'),
	(68, 5, 13, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(69, 5, 13, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(70, 5, 13, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(71, 5, 13, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(72, 5, 89, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-05-09 12:42:50'),
	(73, 5, 13, 42, 34, 1496, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(74, 5, 13, 45, 44, 97768, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-05-15 13:36:18'),
	(76, 5, 13, 45, 3, 6666, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(77, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(78, 5, 13, 45, 6, 13332, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(79, 5, 13, 45, 3, 6666, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(80, 5, 13, 45, 3, 6666, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:38:41'),
	(81, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:41:51'),
	(82, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:41:51'),
	(83, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:41:51'),
	(84, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:41:51'),
	(85, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:41:51'),
	(86, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:41:51'),
	(87, 5, 13, 45, 5, 11110, 'MTUyNDU2NzI3NF85Nzg2', '0000-00-00 00:00:00', '2018-04-26 12:41:51');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.restaurant
CREATE TABLE IF NOT EXISTS `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table restaurant_whiteboard.restaurant: ~7 rows (approximately)
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT INTO `restaurant` (`id`, `user_id`, `name`, `email`, `status`, `created_at`, `updated_at`) VALUES
	(3, 36, 'res_admin restaurant', 'res_admin@gmail.com', 1, '2018-04-03 13:35:12', '2018-04-04 16:43:08'),
	(4, 38, 'res_test', 'res_test@gmail.com', 0, '2018-04-03 13:38:30', '2018-04-06 16:28:19'),
	(5, 60, 'Restaurant User Tokenwise', 'usertoken@email.com', 0, '2018-04-06 17:24:54', '2018-04-06 17:40:56'),
	(13, 89, 'res_azad', '3', 1, '2018-04-23 13:08:19', '2018-04-24 14:49:00'),
	(14, 0, 'newRestaurant', 'userstoken@email.com', 0, '2018-04-24 09:07:32', '2018-04-24 13:07:32'),
	(15, 0, 'newResstauranet', 'abecs@gmail.com', 0, '2018-04-24 09:22:36', '2018-04-24 13:22:36'),
	(16, 0, 'testnewResstauranet', 'testabecs@gmail.com', 0, '2018-04-24 09:26:54', '2018-04-24 13:26:54');
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;

-- Dumping structure for table restaurant_whiteboard.user_role
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hierarchy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table restaurant_whiteboard.user_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `name`, `role_key`, `hierarchy`) VALUES
	(1, 'Super Admin', 'SUPER_ADMIN', 100),
	(2, 'Administrator', 'ADMIN', 90),
	(3, 'User', 'USER', 80);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
