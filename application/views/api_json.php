<?php

$json = json_encode($api_data);
if ($this->input->get_post('callback')) {
    $json = str_replace("\\", "\\\\", $json);
    $json = str_replace("\"", "\\\"", $json);
    $output = $this->input->get_post('callback') . '("' . $json . '")';
} else {
    $output = $json;
}
$this->output
        ->set_content_type('application/json')
        ->set_output($output);
