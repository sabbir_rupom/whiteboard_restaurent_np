
<div class="sidebar-nav">
    <!--<h2>Admin Panel</h2>-->
    <div class="list-group panel">
        <a href="<?= base_url('admin/dashboard') ?>" class="list-group-item list-group-item-success<?= $menu == 'dashboard' ? ' active' : ''; ?>" data-parent="#sidebar-nav">
            <i class="glyphicon glyphicon-search"></i>Dashboard
        </a>
        <a href="#questiion_category" class="list-group-item list-group-item-success<?= $menu == 'qc' ? ' active' : ''; ?>" data-toggle="collapse" data-parent="#sidebar-nav">
            <i class="glyphicon glyphicon-th-large"></i>Quiz Category
        </a>
        <div class="collapse<?= $menu == 'qc' ? ' in' : ''; ?>" id="questiion_category">
            <a href="<?= base_url('admin/category_new') ?>" class="list-group-item">Add New Category</a>
            <a href="<?= base_url('admin/category_list') ?>" class="list-group-item">Show All Categories</a>
        </div>
        <a href="#quiz_questions" class="list-group-item list-group-item-success<?= $menu == 'qq' ? ' active' : ''; ?>" data-toggle="collapse" data-parent="#sidebar-nav">
            <i class="glyphicon glyphicon-tasks"></i>Quiz Questions
        </a>
        <div class="collapse<?= $menu == 'qq' ? ' in' : ''; ?>" id="quiz_questions">
            <a href="<?= base_url('admin/question_new') ?>" class="list-group-item">Add New Question</a>
            <a href="<?= base_url('admin/question_list') ?>" class="list-group-item">Show All Questions</a>
        </div>
        <!--<a href="#quiz_passcodes" class="list-group-item list-group-item-success<?= $menu == 'pc' ? ' active' : ''; ?>" data-toggle="collapse" data-parent="#sidebar-nav">
                <i class="glyphicon glyphicon-qrcode"></i>Quiz Passcode
            </a>
            <div class="collapse<?= $menu == 'pc' ? ' in' : ''; ?>" id="quiz_passcodes">
                <a href="<?= base_url('admin/passcode_new') ?>" class="list-group-item">Add New Passcode</a>
                <a href="<?= base_url('admin/passcode_list') ?>" class="list-group-item">Show All Passcodes</a>
            </div>-->
        <a href="#user_menu" class="list-group-item list-group-item-success<?= $menu == 'um' ? ' active' : ''; ?>" data-toggle="collapse" data-parent="#sidebar-nav">
            <i class="glyphicon glyphicon-user"></i>User Management
        </a>
        <div class="collapse<?= $menu == 'um' ? ' in' : ''; ?>" id="user_menu">
            <a href="<?= base_url('admin/user_new') ?>" class="list-group-item">Add New User</a>
            <a href="<?= base_url('admin/user_list') ?>" class="list-group-item">Show All User</a>
        </div>
        <a href="<?= base_url('admin/settings') ?>" class="list-group-item list-group-item-success<?= $menu == 'st' ? ' active' : ''; ?>" data-parent="#sidebar-nav">
            <i class="glyphicon glyphicon-wrench"></i>Settings
        </a>
    </div>
</div>