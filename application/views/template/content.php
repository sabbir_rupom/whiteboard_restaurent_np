<!--</header>-->
<?php $this->load->view('template/header'); ?>
<!--<header>-->

<!--<main>-->
<div class="container-fluid">
    <div class="row row-fluid">
        <div class="col-sm-2 no-padding sidebar-menu">
            <div class="nav-header-title">
                Admin Panel
            </div>
            <?php $this->load->view('template/navigation'); ?>
        </div>
        <div class="col-sm-10 no-padding main-content">
            <div class="page-header text-right">
                <a class="btn btn-invert" href="<?= base_url('login/logout') ?>"> Log Out <span class="glyphicon glyphicon-off"></span></a>
                <a class="btn btn-warning pull-left side-menu-toggle" href="#" style="margin-left: 15px;"><span class="glyphicon glyphicon-align-justify"></span></a>
            </div>
            <?php $this->load->view($main_content); ?>
        </div>
    </div>
</div>
<!--</main>-->
<script>
    $(function (){
        var slider = 0;
       $('.side-menu-toggle').click(function (){
           $('.sidebar-menu').toggle('slow');
           if(slider == 0) {
               $('.main-content').removeClass("col-sm-10").addClass("col-sm-12", 1000);
               slider = 1;
           } else {
               $('.main-content').removeClass("col-sm-12", 500).addClass("col-sm-10");
               slider = 0;
           }
       })
    });
</script>

<!--<footer>-->
<?php $this->load->view('template/footer'); ?>
<!--</footer>-->