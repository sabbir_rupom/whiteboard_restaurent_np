<?php
ob_start();
header("cache-Control: no-store, no-cache, must-revalidate");
header("cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"><![endif]-->
<!--[if IE 7 ]>    <html class="ie7"><![endif]-->
<!--[if IE 8 ]>    <html class="ie8"><![endif]-->
<!--[if IE 9 ]>    <html class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="description" content="The Quiz">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $title ?></title>
        
        <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>

        <?php
        echo link_tag('css/bootstrap.min.css');
        echo link_tag('css/font-awesome.min.css');
        echo link_tag('css/jquery-ui.min.css');
        echo link_tag('css/admin.css');
        echo link_tag('css/style.css');
        
        echo SCRIPT . base_url('js/jquery-1.11.1.js') . END_SCRIPT;
        echo SCRIPT . base_url('js/bootstrap.min.js') . END_SCRIPT;
        echo SCRIPT . base_url('js/jquery.validate.min.js') . END_SCRIPT;
        echo SCRIPT . base_url('js/jquery-ui.min.js') . END_SCRIPT;
        echo SCRIPT . base_url('js/jquery.twbsPagination.js') . END_SCRIPT;
        echo SCRIPT . base_url('js/custom.js') . END_SCRIPT;

        ?>
        
       
    </head>
    <body>