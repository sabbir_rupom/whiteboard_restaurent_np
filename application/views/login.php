<?php
session_destroy();
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"><![endif]-->
<!--[if IE 7 ]>    <html class="ie7"><![endif]-->
<!--[if IE 8 ]>    <html class="ie8"><![endif]-->
<!--[if IE 9 ]>    <html class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="description" content="The Box">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $title; ?></title>
        <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>

        <?php
        echo link_tag('css/bootstrap.min.css');
        echo link_tag('css/login.css');
        echo SCRIPT . base_url('js/jquery-1.11.1.js') . END_SCRIPT;
        echo SCRIPT . base_url('js/bootstrap.min.js') . END_SCRIPT;
        ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Login Panel</h1>


                    <div class="account-wall">

                        <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
                        <form class="form-signin" method="POST" action="<?= base_url('login'); ?>">
                            <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
                            <input type="password" class="form-control" placeholder="Password" name="password" required>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>