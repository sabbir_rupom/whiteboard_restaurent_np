<div class="row body-content">
    <div class="col-md-12">
        <h2>
            Add New Category
            <?php
            if ($added == 1) {
                echo '<span class="label label-success pull-right">A new category has been added.</span>';
            } else if ($added == -1) {
                echo '<span class="label label-danger pull-right">Category insert failed.</span>';
            }
            ?>
        </h2>
        <hr>
        <form class="form-horizontal" role="form" method="POST" action="<?= base_url('admin/category_new'); ?>">
            <div class="form-group">
                <label class="control-label col-sm-2">Name:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control category_text_1" id="category_name" placeholder="input category" name="category_name"><br>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <input type="submit" class="btn btn-success" name="add_category" value="Add">
                </div>
            </div>
        </form>
    </div>
</div>