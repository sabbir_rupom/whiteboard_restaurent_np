<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <h1>Category List</h1>
        <hr>
        <?php
        if (isset($num_rows) && count($num_rows) > 0) {
            ?>
            <input type="hidden" id="table_name" value="<?= $table_name ?>">

            <table class="table table-bordered">
                <tr>
                    <th>Srl.</th>
                    <th>Category </th>
                    <th>Action</th>
                </tr>
                <tbody>
                    <?php
                    $srl = $page_offset + 1;
                    foreach ($category_list as $value) {
                        ?>
                        <tr class="row-<?= $value->id; ?>">
                            <td><?= $srl; ?></td>
                            <td class="element-<?= $value->id; ?>">
                                <span class="show show-<?= $value->id; ?>"><?= $value->name; ?></span>
                                <input type="text" class="category_text category_text_<?= $value->id; ?>" value="<?= $value->name; ?>">
                            </td>
                            <td>

                                <span class="btn btn-xs btn-primary btn-edit" data-id="<?= $value->id; ?>">Edit</span>
                                <span class="btn btn-xs btn-success btn-update btn-update-<?= $value->id; ?>" data-id="<?= $value->id; ?>">Update</span>
                                <span class="btn btn-xs btn-warning btn-cancel btn-cancel-<?= $value->id; ?>">Cancel</span>
                                <span class="btn btn-xs btn-danger btn-delete" data-id="<?= $value->id; ?>">Delete</span>

                            </td>
                        </tr>
                        <?php
                        $srl++;
                    }
                    ?>
                </tbody>
            </table>
            <?php
            if ($srl > 15) {
                ?>
                <div class="text-center">
                    <ul id="pagination" class="pagination-sm"></ul>
                </div>
                <?php
            }
            ?>
            <br><br>
            <?php
        } else {
            
        }
        ?>
    </div>
</div>

<script>
    $(function () {
        var row_id = 0;
        var table_name = '';
        var category = '';
        $('.btn-cancel, .btn-update, .category_text').hide();
        $('.btn-edit').click(function () {
            row_id = $(this).data("id");
            $('.btn-cancel, .btn-update, .category_text, .show-' + row_id).hide();
            $('.category_text_' + row_id + ', .btn-edit, .btn-update-' + row_id + ', .btn-cancel-' + row_id).show();
            $(this).hide();
        });
        $('.btn-cancel').click(function () {
            $('.btn-cancel, .btn-update, .category_text').hide();
            $('.show, .btn-edit').show();
        });

        $('.btn-delete').click(function () {
            row_id = $(this).data("id");
            table_name = $('#table_name').val();
            if (confirm("Do you wish to delete selected row ?")) {
                var $data = {row_id: row_id, table_name: table_name};
                $.post(base_url + 'admin/ajax_delete_row', $data, function (data) {
                    if (data == 'success') {
                        $('.row-' + row_id).remove();
                    }
                });
            }
        });

        $('.btn-update').click(function () {
            row_id = $(this).data("id");
            table_name = $('#table_name').val();
            category = $('.category_text_' + row_id).val();
            if (confirm("Do you wish to update selected row ?")) {
                var $data = {row_id: row_id, category: category}; // category_jp: category_jp, category_ar: category_ar
                $.post(base_url + 'admin/ajax_update_category', $data, function (data) {
                    if (data == 'success') {
                        $('.show-' + row_id).html(category);
                    } else {
                        alert('Update Failed!')
                    }
                    $('.btn-cancel, .btn-update, .category_text').hide();
                    $('.show, .btn-edit').show();
                });
            }
        });
    });


    var total_row = <?= $num_rows; ?>;
    var limit = <?= $page_limit; ?>;
    var page_offset = parseInt(<?= $page_offset ?>);
    var sort_by = '<?= $sort_by ?>';
    var sort_order = '<?= $sort_order ?>';
    page_offset = page_offset == 0 ? 1 : (Math.floor(page_offset / limit) + 1);
    var total_pages = Math.ceil(total_row / limit);
    window.pagObj = $('#pagination').twbsPagination({
        totalPages: total_pages,
        visiblePages: 7,
        startPage: page_offset,
        onPageClick: function (event, page) {
            console.log(page + ' (from options)');
        }
    }).on('page', function (event, page) {
        var url = base_url + 'admin/category_list/' + sort_by + '/' + sort_order + '/' + ((page - 1) * limit);
        window.location.href = url;
        console.log(page + ' (from event listening)');
    });

</script>
