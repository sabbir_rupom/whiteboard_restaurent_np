<section class="content-header">
    <!--<h1>Over All</h1>-->
</section>

<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><b><?php echo $lang_aside['cng_pass']; ?></b></h3>
                    </div>
                    <div class="border-top"></div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $success = isset($success_msg) ? $success_msg : $this->session->flashdata('success-msg');
                        if ($success) {
                            echo'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                            echo $success;
                            echo'</div>';
                        }
                        $error = $this->session->flashdata('error-msg');
                        if (isset($error)) {
                            echo'<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
                            echo ' ' . $error;
                            echo'</div>';
                        }
                        ?>
                        <form role="form" action="<?php echo base_url('login/change_password') ?>" method="post" id="changePassword" autocomplete="off">
                            <div class="input-group margin-bottom">
                                <input type="hidden" name="uid" value="<?php echo $uid; ?>">
                                <span class="input-group-addon">New Password: </span>
                                <input type="password" name="password" id="password" class="form-control" placeholder="New Password" required="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open" aria-hidden="true" onclick="showPassword(this)"></span></span>
                            </div>
                            <input type="submit" id="btn-changePassword" class="btn btn-custom" value="<?php echo $lang_forms['update']; ?>">
                            <?php echo anchor('/', $lang_forms['cancel'], ['class' => 'btn btn-danger']); ?>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->