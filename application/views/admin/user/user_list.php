<section class="content-header">
    <!--<h1>Over All</h1>-->
</section>

<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><b></b></h3>
                    </div>
                    <div class="border-top"></div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $success = isset($success_msg) ? $success_msg : $this->session->flashdata('success-msg');
                        if ($success) {
                            echo'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                            echo $success;
                            echo'</div>';
                        }
                        $error = $this->session->flashdata('error-msg');
                        if (isset($error)) {
                            echo'<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
                            echo ' ' . $error;
                            echo'</div>';
                        }
                        ?>
                        <table class="table table-bordered table-striped" id="couponTable"> <!-- for data table id="example1" -->
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>User Name</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                if (!empty($user_list)) {
                                    foreach ($user_list as $key => $user) {
                                        if ($user->role == 1) {
                                            continue;
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $user->full_name; ?></td>
                                            <td><?php echo $user->email; ?></td>
                                            <td><?php echo $user->username; ?></td>
                                            <td><?php echo $user->role_name; ?></td>
                                            <td>
                                                <?php
                                                $userRole = $this->session->userdata('role');
                                                $username = $this->session->userdata('username');
                                                if ($userRole == 1 || $username == $user->full_name) {
                                                    ?>
                                                    <!--<a href="<?php echo base_url('login/change_password/' . $user->id); ?>">Password change </a> |--> 
                                                    <a href="#<?php //echo base_url('admin/user_edit/' . $user->id); ?>"><i class="fa fa-pencil-square-o"></i> </a> | 
                                                    <a onclick="return confirm('Do you want to delete this User ?')" href="<?php echo base_url('admin/user_delete/' . $user->id); ?>"> 
                                                        <i class="glyphicon glyphicon-trash"></i> </a>
                                                    <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->