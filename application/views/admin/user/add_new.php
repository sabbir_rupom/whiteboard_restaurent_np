<section class="content-header">
    <!--<h1>Over All</h1>-->
</section>
<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><b></b></h3>
                    </div>
                    <div class="border-top"></div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $success = isset($success_msg) ? $success_msg : $this->session->flashdata('success-msg');
                        if ($success) {
                            echo'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                            echo $success;
                            echo'</div>';
                        }
                        $error = $this->session->flashdata('error-msg');
                        if (isset($error)) {
                            echo'<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
                            echo ' ' . $error;
                            echo'</div>';
                        }
                        ?>
                        <form role="form" action="<?php echo base_url('admin/user_new') ?>" method="post" id="user_add" autocomplete="off">
                            <div class="input-group">
                                <span class="input-group-addon">Full Name: </span>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name Example" required="">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">Email: </span>
                                <input onkeyup="check_unique_email()" type="email" name="email" id="email" class="form-control" placeholder="name@example.com" required="">
                            </div>
                            <span id="unique_email_req"></span>
                            <div class="input-group">
                                <span class="input-group-addon">User Name: </span>
                                <input onkeyup="check_unique_user()" type="text" name="user_name" id="user_name" class="form-control" placeholder="name.example." required="">
                            </div>
                            <span id="unique_user_req"></span>
                            <div class="input-group">
                                <span class="input-group-addon">Password: </span>
                                <input type="password" name="password" id="password" class="form-control" placeholder="" required="" minlength="5">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-eye-close" aria-hidden="true" onclick="showPassword(this)"></span></span>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">User Role: </span>
                                <div class="input-group-btn" data-toggle="buttons">
                                    <?php
                                    foreach ($roles as $i => $role) {
                                        echo '<label class="btn btn-primary' . ($i == 0 ? ' active' : '') . '" ><input type="radio" name="user_role" ' . ($i == 0 ? 'checked' : '') . ' value="' . $role->id . '"  autocomplete="off"> ' . $role->name . '</label>';
                                    }
//                                    
                                    ?>
                                </div>
                            </div>
                            <input type="submit" id="btn-login" class="btn btn-custom btn-primary" value="Add User">
                            <?php // echo anchor('#', $lang_forms['cancel'], ['class' => 'btn btn-danger']);  ?>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->