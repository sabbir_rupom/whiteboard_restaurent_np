<section class="content-header">
    <!--<h1>Over All</h1>-->
</section>
<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><b><?php echo $lang_user['title_edit']; ?></b></h3>
                    </div>
                    <div class="border-top"></div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $success = isset($success_msg) ? $success_msg : $this->session->flashdata('success-msg');
                        if ($success) {
                            echo'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                            echo $success;
                            echo'</div>';
                        }
                        $error = $this->session->flashdata('error-msg');
                        if (isset($error)) {
                            echo'<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
                            echo ' ' . $error;
                            echo'</div>';
                        }
                        ?>
                        <form role="form" action="<?php echo base_url('user/edit') ?>" method="post" id="userEdit" autocomplete="off">
                            <div class="input-group">
                                <span class="input-group-addon"><?php echo $lang_user['name']; ?>: </span>
                                <input type="hidden" name="id" value="<?php echo $user->id; ?>">
                                <input type="text" name="name" id="name" value="<?php echo $user->name; ?>" class="form-control" placeholder="<?php echo $lang_user['name']; ?>" required="">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><?php echo $lang_user['email']; ?>: </span>
                                <input type="email" name="email" id="email" value="<?php echo $user->email; ?>" class="form-control" readonly="">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><?php echo $lang_user['role']; ?>: </span>
                                <div class="input-group-btn" data-toggle="buttons">
                                    <?php
                                    foreach ($roles as $role) {
                                        echo '<label class="btn btn-primary' . ($role->id == $user->role ? ' active' : '') . '" ><input type="radio" name="user_role" ' . ($role->id == $user->role ? 'checked' : '') . ' value="' . $role->id . '"  autocomplete="off"> ' . $role->name . '</label>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <input type="submit" id="btn-updateUser" class="btn btn-custom" value="<?php echo $lang_forms['update']; ?>">
                        <?php echo anchor('/user', $lang_forms['cancel'], ['class' => 'btn btn-danger']); ?>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->