<div class="row">
    <div class="col-lg-10 col-lg-offset-1">

        <h1>Question List</h1>
        <hr><br>
        <form class="form-horizontal" role="form" method="POST" action="<?= base_url('admin/question_list'); ?>">
            <div class="form-group">
                <label class="control-label col-sm-2">Select Category:</label>
                <div class="col-sm-3" style="padding-top: 7px;">
                    <select id="input_category_id" name="category_id">
                        <option value="0">--- Please Select ---</option>
                        <?php
                        $category_name = '';
                        foreach ($category_list as $value) {
                            if ($category_id == $value->id) {
                                $category_name = $value->name;
                            }
                            echo '<option value="' . $value->id . '" ' . ($category_id == $value->id ? 'selected' : '') . '>' . $value->name . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="col-sm-4">
                    <input type="submit" class="btn btn-info" name="submit_category" value="Show"> 
                </div>
            </div>
        </form>
        <?php ?>
        <h2> Showing All Questions of <?= $category_name ?> </h2>
        <table class="table table-striped table-responsive table-questions">
            <tr>
                <th>#</th>
                <th>Question</th>
                <th>Answer</th>
                <th>Action</th>
            </tr>
            <tbody>
                <?php
                $cnt = count($question_list);
                if ($cnt > 0) {
                    $srl = $page_offset;
                    foreach ($question_list as $value) {
                        $srl++;
                        ?>
                        <tr>
                            <td><?= $srl; ?></td>
                            <td>
                                <a href="#" data-toggle="modal" data-target=".<?php echo $srl; ?>">
                                    <img class="img img-responsive question-list-img center-block" src="<?= base_url(QUESTION_IMAGE_URL . $value->question_image); ?>" >
                                </a>
                                <!--  Modal content for the mixer image example -->
                                <div class="modal <?php echo $srl; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <img class="img img-responsive question-modal-img" src="<?= base_url(QUESTION_IMAGE_URL . $value->question_image); ?>" >
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal mixer image -->
                            </td>
                            <td><?= $value->answer; ?></td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="<?= base_url('admin/question_edit' . '/' . $value->id); ?>">Edit</a>
                                <a class="btn btn-xs btn-warning" href="<?= base_url('admin/question_view' . '/' . $value->id); ?>">View</a>
                                <a class="btn btn-xs btn-danger" onclick="return confirm('Do you want to delete this Question ?')" href="<?= base_url('admin/question_delete' . '/' . $value->id . '/' . $value->question_image); ?>">Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <?php if($cnt > 0) { ?>
        <div class="text-center">
            <ul id="pagination" class="pagination-sm"></ul>
        </div>
        <?php } ?>
    </div>
</div>

<script>
    var total_row = <?= $total_row; ?>;
    var limit = <?= $page_limit; ?>;
    var page_offset = parseInt(<?= $page_offset ?>);
    var cat_id = <?= $category_id ?>;
    page_offset = page_offset == 0 ? 1 : (Math.floor(page_offset / limit) + 1);
    var total_pages = Math.ceil(total_row / limit);
    window.pagObj = $('#pagination').twbsPagination({
        totalPages: total_pages,
        visiblePages: 7,
        startPage: page_offset,
        onPageClick: function (event, page) {
            console.log(page + ' (from options)');
        }
    }).on('page', function (event, page) {
        var url = base_url + 'admin/question_list/' + cat_id + '/' + limit + '/' + ((page - 1) * limit);
        window.location.href = url;
        console.log(page + ' (from event listening)');
    });
</script>
