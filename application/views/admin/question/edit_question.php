<div class="row body-content question-edit">
    <div class="col-md-6">
        <h2>
            Edit Question
        </h2>
        <hr>
        <form class="form-horizontal" id="question_form" role="form" method="POST" action="<?= base_url('admin/question_update'); ?>" enctype="multipart/form-data" accept-charset="utf-8" >
            <div class="form-group">
                <label class="control-label col-sm-3">Select Category*:</label>
                <div class="col-sm-4" style="padding-top: 7px;">
                    <select id="input_category_id" name="category_id">
                        <option value="0">--- Please Select ---</option>
                        <?php foreach ($category_list as $value) { ?>
                            <option value="<?= $value->id; ?>" <?= $question_data->category_id == $value->id ? 'selected' : '' ?>> <?= $value->name; ?> </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Image*:</label>
                <div class="col-sm-3">
                    <input type="file" class="lang-element-image" name="image" id="input_image_en">
                </div>
            </div>
            <div class="row question-img">
                <div class="col-sm-4 col-sm-offset-3 image-view"><img class="img img-responsive question-edit-img" src="<?= base_url(QUESTION_IMAGE_URL . $question_data->question_image); ?>" ></div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Answer*:</label>
                <div class="col-sm-4">
                    <!--<input type="text" class="form-control lang-element-answer" placeholder="Enter answer" name="answer" id="input_answer_en" required="" value="<?= $question_data->answer; ?>">-->
                    <select class="form-control" name="answer" id="input_answer">
                        <option value="A" selected="">Select Answer</option>
                        <option value="A"<?= $question_data->answer == 'A' ? ' selected' : ''; ?>>A</option>
                        <option value="B"<?= $question_data->answer == 'B' ? ' selected' : ''; ?>>B</option>
                        <option value="C"<?= $question_data->answer == 'C' ? ' selected' : ''; ?>>C</option>
                        <option value="D"<?= $question_data->answer == 'D' ? ' selected' : ''; ?>>D</option>
                        <option value="E"<?= $question_data->answer == 'E' ? ' selected' : ''; ?>>E</option>
                        <option value="F"<?= $question_data->answer == 'F' ? ' selected' : ''; ?>>F</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <input type="hidden" name="question_id" value="<?= $question_data->id ?>">
                    <input type="hidden" name="old_img" value="<?= $question_data->question_image ?>">
                    <input type="submit" class="btn btn-success" name="update_question" value="Update Question">
                    <button type="button" class="btn btn-warning" onclick="history.go(-1);">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>
