<div class="row body-content">
    <div class="col-md-8">
        <h2>
            Add New Question
            <?php
            if ($added == 1) {
                echo '<span class="label label-success pull-right">A new question has been added.</span>';
            }
            ?>
            <?php
            $success = isset($success_msg) ? $success_msg : $this->session->flashdata('success-msg');
            if ($success) {
                echo'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                echo $success;
                echo'</div>';
            }
            $error = $this->session->flashdata('error-msg');
            if (isset($error)) {
                echo'<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>';
                echo ' ' . $error;
                echo'</div>';
            }
            ?>
        </h2>
        <hr>
        <form class="form-horizontal question-add" id="question_form" role="form" method="POST" action="<?= base_url('admin/question_new'); ?>" enctype="multipart/form-data" accept-charset="utf-8">
            <div class="form-group">
                <label class="control-label col-sm-3">Select Category*:</label>
                <div class="col-sm-4" style="padding-top: 7px;">
                    <select id="input_category_id" name="category_id" required="" class="form-control" >
                        <option value="0">--- Please Select ---</option>
                        <?php foreach ($category_list as $value) { ?>
                            <option value="<?= $value->id; ?>"><?= $value->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Image*:</label>
                <div class="col-sm-3">
                    <input type="file" class="lang-element-image" name="image" id="input_image_en" required="">
                </div>
            </div>
            <div class="row question-img">
                <div class="col-sm-4 col-sm-offset-3 image-view"></div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Answer*:</label>
                <div class="col-sm-3">
                    <!--<input type="text" class="form-control lang-element-answer" placeholder="Enter answer" name="answer" id="input_answer_en" required="">-->
                    <select class="form-control" name="answer" id="input_answer">
                        <option value="A" selected="">Select Answer</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="F">F</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <input type="submit" class="btn btn-success" name="add_question" value="Add Question">
                </div>
            </div>
        </form>
    </div>
</div>
