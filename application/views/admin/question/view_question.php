<div class="row body-content">
    <div class="col-md-7">
        <h2>Question Details</h2>
        <hr>
        <table class="table table-responsive table-bordered table-striped">
            <tr>
                <th>Category Name</th>
                <td><?= $question_details->name; ?></td>
            </tr>
            <tr>
                <th>Question</th>
                <td>
                    <a href="#" data-toggle="modal" data-target=".image_modal">
                        <img class="img img-responsive question-list-img" src="<?= base_url(QUESTION_IMAGE_URL . $question_details->question_image); ?>" >
                    </a>
                    <!--  Modal content for the mixer image example -->
                    <div class="modal image_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>
                                </div>
                                <div class="modal-body">
                                    <img class="img img-responsive question-modal-img" src="<?= base_url(QUESTION_IMAGE_URL . $question_details->question_image); ?>" >
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal mixer image -->
                </td>
            </tr>
            <tr>
                <th>Answer</th>
                <td><?= $question_details->answer; ?></td>
            </tr>
        </table>
        <div class="center-block">
            <a class="btn btn-primary" href="<?= base_url('admin/question_edit') . '/' . $question_details->question_id; ?>">Edit Question</a>
        </div>
    </div>
</div>
