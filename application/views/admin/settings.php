<div class="row body-content">
    <div class="col-md-12">
        <h2>Admin Settings</h2>
        <hr>
        <?php
        
        switch ($msg) {
            case 1:
                echo '<span class="label label-success">Password has been changed.</span>' . br(2);

                break;
            case 0:
                echo '<span class="label label-danger">Type old password correctly.</span>' . br(2);

                break;
            case -1:
                echo '<span class="label label-danger">New password is empty.</span>' . br(2);

                break;

            default:
                break;
        }
        ?>
        <form class="form-horizontal" role="form" method="POST" action="<?= base_url('admin/settings'); ?>">
            <div class="form-group">
                <label class="control-label col-sm-2">Original Password:</label>
                <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="Current Password" name="current_password">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Change Password:</label>
                <div class="col-sm-7">
                    <input type="password" class="form-control" placeholder="New Password" name="new_password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-7">
                    <input type="submit" class="btn btn-info" name="change_password" value="Change">
                </div>
            </div>
        </form>
    </div>
</div>
