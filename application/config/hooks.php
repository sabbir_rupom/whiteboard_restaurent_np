<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	https://codeigniter.com/user_guide/general/hooks.html
  |
 */
//$CI = &get_instance();
// if($this->CI->router->class=="C_api") {
     
    $hook['pre_controller'][] = array(
        'class' => 'Api_Exception',
        'function' => 'SetExceptionHandler',
        'filename' => 'Api_Exception.php',
        'filepath' => 'hooks/exception'
    );

    $hook['post_controller'][] = array(
        'class' => 'Response',
        'function' => 'sendResponse',
        'filename' => 'Response.php',
        'filepath' => 'hooks'
    );
//}