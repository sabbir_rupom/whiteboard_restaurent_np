<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'C_admin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin/(:any)/(:any)/(:any)/(:any)'] = "C_admin/$1/$2/$3/$4";
$route['admin/(:any)/(:any)/(:any)'] = "C_admin/$1/$2/$3";
$route['admin/(:any)/(:any)'] = "C_admin/$1/$2";
$route['admin/(:any)'] = "C_admin/$1";
$route['admin'] = "C_admin";

$route['examples'] = "C_examples";

$route['login/(:any)/(:any)'] = "C_login/$1/$2";
$route['user-login'] = "C_login/user_login";
$route['login/(:any)'] = "C_login/$1";
$route['login'] = "C_login";

$route['get-category-questions'] = "C_api/get_category_questions";
$route['save-restaurant'] = "C_api/save_restaurant";
$route['save-order'] = "C_api/save_order";
$route['get-restaurant'] = "C_api/get_restaurant";
$route['get-order'] = "C_api/get_order";
$route['get-menu'] = "C_api/get_menu";
$route['save-menu'] = "C_api/save_menu";
$route['get-categories'] = "C_api/get_categories";
$route['get-user'] = "C_api/get_user";
$route['console'] = "C_login/console";
$route['download/(:any)'] = "C_api/download/$1";