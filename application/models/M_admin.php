<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once 'B_model.php';

class M_admin extends B_model {

    public function __construct() {
        parent::__construct();
    }

    public function verify_admin_credential($user, $pass) {
        $query = $this->db->where('username', $user)->where('password', $pass)->get('admin_user');
        $full_name = $user_role = '';
        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $full_name = $row->full_name;
                $user_role = $row->role;
                break;
            }

            $user_data = array(
                'username' => $full_name,
                'role' => $user_role,
                'admin' => TRUE
            );

            $this->session->set_userdata($user_data);

            return TRUE;
        }
        return FALSE;
    }

    public function verify_passcode($passcode) {
        $query = $this->db->where('passcode', $passcode)->get('passcodes');
        if ($query->num_rows() == 1) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_question_details($row_id = 0) {
        return $this->db->select('c.*, q.id as question_id, question_image, answer')
                        ->join('categories as c', 'c.id = q.category_id', 'LEFT')
                        ->where('q.id', $row_id)
                        ->get('questions as q')
                        ->result();
    }

    public function get_common_data() {
        $result = array();
        $result['categories'] = $this->db->select('id')->count_all('categories');
        return $result;
    }

    public function change_admin_password($old, $new) {
        $result = $this->db->where('password', $old)->count_all_results('admin_user');

        if ($result > 0) {
            $username = $this->session->userdata('username');
            $this->db->where('full_name', $username)->update('admin_user', array('password' => $new));
            return 1;
        } else {
            return 0;
        }
    }

    public function create_user() {
        $insertData = [
            'full_name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('user_name'),
            'password' => md5($this->input->post('password')),
            'role' => $this->input->post('user_role'),
            'created_at' => CURR_TIME,
        ];
        $insert_id = $this->insert_into_table('admin_user', $insertData);
        return $insert_id;
    }

    public function get_user_roles($where = null) {
        return $this->db->get_where('user_role', $where)->result();
    }

    public function user_list() {
        $query = $this->db->select('u.*, ur.name as role_name')
                ->join('user_role' . ' as ur', 'ur.id = u.role', 'left')
                ->order_by('u.id', 'DESC')
                ->get_where('admin_user' . ' as u');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

}
