<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once 'B_model.php';

class M_app extends B_model {

    public function __construct() {
        parent::__construct();
    }

    public function get_question_categories() {
        $categories = array();
        $sql = "select c.*, count(q.id) as question_cnt from categories as c "
                . "LEFT JOIN questions as q on q.category_id = c.id group by q.category_id order by c.id asc;";
        $fetch_data = $this->db->query($sql);
        if ($fetch_data->num_rows() > 0) {
            $result = $fetch_data->result_array();
            foreach ($result as $row) {
                $hash = array();
                $hash['id'] = intval($row['id']);
                $hash['name'] = $row['name'];
                $hash['question_cnt'] = intval($row['question_cnt']);
                $categories[] = $hash;
            }
        }

        return $categories;
    }

    public function get_questions_by_category($category_id = 0) {
        $q_images = $questions = array();
        $fetch_questions = $this->db->where('category_id', $category_id)->get('questions');
        if ($fetch_questions->num_rows() > 0) {
            $result = $fetch_questions->result_array();
                foreach ($result as $row) {
                    $hash = array();
                    $hash['question_id'] = (int) $row['id'];
                    $q_images[$hash['question_id']] = $row['question_image'];
                    
                    $hash['answer'] = $row['answer'];
                    $questions[] = $hash;
                }
        }
        return array($questions, $q_images);
    }
    
    public function auth_session_update($userID = 0) {
        $token = $this->generateToken();
        $user = $this->db->get_where('authsessions', array('userid' => $userID));
        if ($user->num_rows() == 1) {
            $this->db->update('authsessions', array('tokenid' => $token), array('userid' => $userID));
        } else {
            $this->db->insert('authsessions', array('userid' => $userID, 'tokenid' => $token, 'created_at' => CURR_DATETIME));
        }
        
        return $token;
    }
    
    public function save_restaurant($restaurant_id = 0, $restaurant_name = "", $email = "") {
        if ($restaurant_id > 0):

            $id = $this->update_table('restaurant', array('name' => $restaurant_name, 'email'=>$email), array('id' => $restaurant_id));
        
            $where = array(
                'id' => $restaurant_id
            );
            $result = $this->m_app->get_table_row('restaurant', $where);
        else:

            $id = $this->insert_into_table('restaurant', array('name' => $restaurant_name, 'email'=>$email, 'created_at' => CURR_DATETIME));
            $where = array(
                'id' => $id
            );
            $result = $this->m_app->get_table_row('restaurant', $where);
        endif;
        
//        $srcfile = 'project.zip';
//        $dstfile = './'.$restaurant_id.'/project.zip';
//        $destination = './'.$restaurant_id.'/';
//        mkdir(dirname($dstfile), 0777, true);
//        copy($srcfile, $dstfile);
//        
//        $zip = new ZipArchive;
//        $res = $zip->open('./'.$restaurant_id.'/project.zip');
//        if ($res === TRUE) {
//          @$zip->extractTo($destination);
//          $zip->close();
//          unlink($dstfile);
//          //echo 'Extract done!';
//        } else {
//          //echo 'Extract done!';
//        }


        return $result;
    }
    public function save_menu($userId, $menu_id = 0, $restaurant_id = 0, $restaurant_menu = "", $menu_desc="", $price = 0) {
        
        //throw new Api_Exception(Result_code::USER_ACCESS_FORBIDDEN, 'Forbidden');
        if($this->authorize_current_user($userId, $restaurant_id)){
            if ($menu_id > 0):

            $id = $this->update_table('menu', array('restaurant_id' => $restaurant_id, 'name' => $restaurant_menu, 'description'=>$menu_desc, 'price'=>$price), array('id' => $menu_id));
        
            $where = array(
                'id' => $menu_id
            );
            $result = $this->get_table_row('menu', $where);
        else:

            $id = $this->insert_into_table('menu', array('restaurant_id' => $restaurant_id, 'name' => $restaurant_menu, 'price'=>$price, 'description'=>$menu_desc, 'created_at' => CURR_DATETIME));
            $where = array(
                'id' => $id
            );
            $result = $this->get_table_row('menu', $where);
        endif;
        
        return $result;
        }
        
        else{
            throw new Api_Exception(Result_code::USER_ACCESS_FORBIDDEN, 'Forbidden');
        }
        
    }
    
    public function save_order($userId, $menu_id = 0, $restaurant_id = 0, $unit = 0) {
        
        $token_id = $this->get_table_row('authsessions', ["userid"=>$userId]);
        $menu = $this->get_table_row('menu', ["id"=>$menu_id]);
        //return $token_id->tokenid;
        
            $total_price = $unit*$menu->price;
            $id = $this->insert_into_table('orders', array('user_id' => $userId, 'restaurant_id' => $restaurant_id, 'menu_id'=>$menu_id, 'unit' => $unit, 'total_price'=>$total_price, 'tokenid'=>$token_id->tokenid, 'created_at' => CURR_DATETIME));
        
            $where = array(
                'id' => $id
            );
            $result = $this->get_table_row('orders', $where);

        
        return $result;
        
    }

}
