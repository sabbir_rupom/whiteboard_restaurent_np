<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Response {

    public static $result = '';
    public static $status_header = 200;

    public static function sendResponse() {

        //self::$result['time'] = date("Y-m-d H:i:s");
        $CI = & get_instance();
        if ($CI->router->class == "C_api") {

            $CI->output
                    ->set_status_header(self::$status_header)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode(self::camelizeJsonIndex(self::$result), JSON_NUMERIC_CHECK))
                    ->_display();
            exit;
        }
    }

    /**
     * Camelize Josn Index
     * @param array $json_array
     * @return array
     */
    public static function camelizeJsonIndex($json_array) {
        if (is_array($json_array) > 0) {
            foreach ($json_array as $key => $value) {
                if (is_object($value)) {
                    $value = get_object_vars($value);
                }
                if (is_array($value)) {
                    $value = self::camelizeJsonIndex($value);
                }
                $new_key = '';
                foreach (explode('_', $key) as $i => $v) {
                    $new_key .= $i == 0 ? $v : ucfirst($v);
                }
                unset($json_array[$key]);
                $json_array[$new_key] = $value;
                //var_dump($new_key);exit;
            }
        }

        return $json_array;
    }

}
