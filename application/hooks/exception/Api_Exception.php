<?php

(defined('BASEPATH')) OR exit('Forbidden 403');

/**
 * API Exception
 */
include_once APPPATH . 'hooks/Response.php';

class Api_Exception extends Exception {

    protected $title;
    public function __construct($code = 0, $message = '', $title = '', Exception $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->title = $title;
    }

    public function SetExceptionHandler() {
        set_exception_handler(array($this, 'HandleExceptions'));
    }

    public function HandleExceptions($e) {
        if ($e instanceof Api_Exception) {
            $code = $e->getCode();
            $result = array(
                "error" => array(
                    'title' => $e->getMessage(),// $this->title,
                    'message' => Result_code::getMessage($code)
                )
            );
        } else {
            $result = array(
                "error" => array(
                    'title' => $e->getMessage() ,//$this->title,
                    'message' => Result_code::getMessage($code)
                )
            );
        }
        //if (Common_Util_ConfigUtil::getInstance()->isErrorDump()) {
//        $result['error_dump'] = array(
//            'code' => $e->getCode(),
//            'file' => $e->getFile(),
//            'line' => $e->getLine()
//        );
        //}
        Response::$status_header = 401;
        Response::$result = $result;
        Response::sendResponse();
    }

}
