<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include_once 'B_controller.php';

/**
 * @property M_admin $m_admin
 */
class C_examples extends B_controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_admin');

        if ($this->session->has_userdata('username')) {
            $this->admin = TRUE;
        } else {
            redirect('login');
        }
    }

    public function index() {
        $data['main_content'] = 'admin/dashboard';
            $data['title'] = 'Smart Lock | Admin';
            $data['menu'] = 'dashboard';
            $data['admin'] = $this->admin;
            $this->load->view(THEMES_ADMIN, $data);

        //$this->dashboard();
    }

    /*
     * -----------------------------------------------------------------
     *  Banner Section
     * -----------------------------------------------------------------
     */

    public function dashboard() {
        //if ($this->admin) {
            $data = array();
            $result = $this->m_admin->get_common_data();
            $data['categories'] = $result['categories'];
            $data['main_content'] = 'admin/dashboard';
            $data['title'] = 'Smart Lock | Admin';
            $data['menu'] = 'dashboard';
            $data['admin'] = $this->admin;
            $this->load->view(THEMES_ADMIN, $data);
        //}
    }

    public function category_new($added = 0) {
        if ($this->input->post('add_category')) {
            $added = 1;
            $name = trim($this->input->post('category_name'));
            $time = date('Y-m-d h:i:s');

            if ($name == '') { // && $name_jp == '' && $name_ar == '') {
                $added = -1;
            } else {
                $insert_data = array(
                    'name' => $name,
                    'created_at' => $time
                );

                $table_name = 'categories';
                $insert_id = $this->m_admin->insert_into_table($table_name, $insert_data);
            }
        }
        $data['main_content'] = 'admin/category/add_new';
        $data['title'] = 'Smart Lock Admin';
        $data['added'] = $added;
        $data['menu'] = 'qc';
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function category_list($sort_by = 'id', $sort_order = 'asc', $offset = 0) {
        $limit = 15;
        $table_name = 'categories';
        $filter = '';
        $result = $this->m_admin->get_table_filtered_data($table_name, $filter, $limit, $offset, $sort_by, $sort_order);

        $data['table_name'] = $table_name;
        $data['category_list'] = $result['data'];
        $data['num_rows'] = $result['num_rows'];
        $data['page_limit'] = $limit;
        $data['page_offset'] = $offset;
        $data['page_offset'] = $offset;
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;

        $data['main_content'] = 'admin/category/show_list';
        $data['title'] = 'Smart Lock Admin';
        $data['menu'] = 'qc';
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function ajax_update_category() {
        if ($this->input->post('row_id')) {
            $row_id = intval($this->input->post('row_id'));
            $category = trim($this->input->post('category'));
            $table_name = 'categories';
            $this->m_admin->update_table_with_id($table_name, array('name' => $category), $row_id);

            echo 'success';
        }
    }

    public function question_new($added = 0) {
        if ($this->input->post('add_question') && $this->input->post('category_id')) {
            $added = 1;
            if (!empty($_FILES['image']['name'])) {
                if (!file_exists(QUESTION_IMAGE_PATH)) {
                    mkdir(QUESTION_IMAGE_PATH, 0777, true);
                }
                $image = $this->_uploadFiles(QUESTION_IMAGE_PATH, 'image')["file_name"]; // Upload path, Input fild name, Upload Image name, Array key file_name
            }
            $time = date('Y-m-d h:i:s');
            $insert_question = [
                'category_id' => intval($this->input->post('category_id')),
                'question_image' => $image,
                'answer' => trim($this->input->post('answer')),
                'created_at' => $time
            ];
            $this->m_admin->insert_into_table('questions', $insert_question);
        }
        $data['category_list'] = $this->m_admin->get_table_data('categories');
        $data['main_content'] = 'admin/question/add_new';
        $data['title'] = 'Smart Lock Admin';
        $data['added'] = $added;
        $data['menu'] = 'qq';
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function question_list($category_id = 0, $limit = 10, $offset = 0) {
        $table_name = 'questions';
        $numrows = 0;
        if ($this->input->post('submit_category')) {
            $category_id = intval($this->input->post('category_id'));
        }
        $filter = 'category_id = ' . $category_id;
        $result = $this->m_admin->get_table_filtered_data($table_name, $filter, $limit, $offset);
        $numrows = $result['num_rows'];
        $data = array(
            'question_list' => $result['data'],
            'category_id' => $category_id,
            'page_limit' => $limit,
            'page_offset' => $offset,
            'total_row' => $numrows,
            'category_list' => $this->m_admin->get_table_data('categories'),
            'main_content' => 'admin/question/show_list',
            'title' => 'Smart Lock Admin',
            'menu' => 'qq'
        );

        $this->load->view(THEMES_ADMIN, $data);
    }

    public function question_edit($id = 0) {
        $row_id = intval($id);
        $data = array(
            'category_list' => $this->m_admin->get_table_data('categories'),
            'question_data' => $this->m_admin->get_table_row_with_id('questions', $row_id),
            'main_content' => 'admin/question/edit_question',
            'title' => 'Smart Lock Admin',
            'menu' => 'qq'
        );
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function question_view($id = 0) {
        $row_id = intval($id);
        $result = $this->m_admin->get_question_details($row_id);
        $data = array(
            'question_details' => $result[0],
            'main_content' => 'admin/question/view_question',
            'title' => 'Smart Lock Admin',
            'menu' => 'qq'
        );
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function question_update() {
        if ($this->input->post('question_id') && $this->input->post('category_id')) {
            $question_id = intval($this->input->post('question_id'));
            $image = $this->input->post('old_img');
            // New Image up and old image delete into folder
            // $image old image
            if (!empty($_FILES['image']['name'])) {
                @unlink(QUESTION_IMAGE_PATH . $image);
                $image = $this->_uploadFiles(QUESTION_IMAGE_PATH, 'image')["file_name"]; // Upload path, Input fild name, As your wish Upload Image name, Array key file_name
                // $image new image
            }
            $update_question = array(
                'category_id' => intval($this->input->post('category_id')),
                'question_image' => $image, //if upload new image $image=new image else $image=old image
                'answer' => trim($this->input->post('answer')),
            );

            $this->m_admin->update_table_with_id('questions', $update_question, $question_id);
            redirect('admin/question_view/' . $question_id);
        }
    }

    public function question_delete($id, $image) {
        $affected_rows = $this->m_admin->delete_table_row('questions', ['id' => $id]);
        if (isset($affected_rows)) {
            @unlink(QUESTION_IMAGE_PATH . $image);
        }
        redirect('admin/question_list');
    }

    private function _set_pagination($url, $num_rows, $limit, $sort_by, $sort_order, $uri_seg) {
        $page_config['base_url'] = base_url() . $url;
        $page_config['total_rows'] = $num_rows;
        $page_config['per_page'] = $limit;
        $page_config['num_links'] = 7;
        $page_config['uri_segment'] = $uri_seg;
        $page_config['full_tag_open'] = '<div id="pagination">';
        $page_config['full_tag_close'] = '</div>';
        $page_config['next_link'] = 'next ›';
        $page_config['last_link'] = 'Last »';
        $page_config['prev_link'] = '‹ prev';
        $page_config['first_link'] = '« First';
        $page_config['anchor_class'] = 'onclick="return change_page(this)"';


        $this->pagination->initialize($page_config);
    }

    public function edit_table_row($table = '', $id = 0) {
        if ($table != '' && $id > 0) {
            $row_id = intval($id);
            $data['column_list'] = $this->m_admin->get_table_columns($table);
            $data['row_data'] = $this->m_admin->get_table_row_with_id($table, $row_id);
            $data['table'] = $table;
            $data['main_content'] = 'admin/edit_table_row';
            $data['title'] = 'Smart Lock | Admin';
            $data['menu'] = '';
            $this->load->view(THEMES_ADMIN, $data);
        }
    }

    public function ajax_delete_row() {
        if ($this->input->is_ajax_request()) {
            $row_id = intval($this->input->post('row_id'));
            $table_name = trim($this->input->post('table_name'));
            $this->m_admin->delete_table_row($table_name, 'id = ' . $row_id);
            echo 'success';
        }
    }

    public function settings() {
        $data['msg'] = 2;
        if ($this->input->post('change_password')) {
            if ($this->input->post('new_password') == '') {
                $data['msg'] = -1;
            } else {
                $old_pass = md5($this->input->post('current_password'));
                $new_pass = md5($this->input->post('new_password'));

                $data['msg'] = $this->m_admin->change_admin_password($old_pass, $new_pass);
            }
        }

        $data['main_content'] = 'admin/settings';
        $data['title'] = 'Smart Lock | Admin';
        $data['menu'] = 'st';
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function check_unique_email() {
        $email = $this->input->post('email');
        $email_count = $this->m_admin->check_unique('admin_user', ['email' => $email]);
        if ($email_count > 0) {
            echo 2;
        } else {
            echo 1;
        }
    }

    public function check_unique_user() {
        $user_name = $this->input->post('user_name');
        $user_name_count = $this->m_admin->check_unique('admin_user', ['username' => $user_name]);
        if ($user_name_count > 0) {
            echo 2;
        } else {
            echo 1;
        }
    }

    public function user_new() {
        if ($this->requestMethod == 'POST') {
            $email = $this->input->post('email');
            $user_name = $this->input->post('user_name');
            $email_count = $this->m_admin->check_unique('admin_user', ['email' => $email]);
            $user_name_count = $this->m_admin->check_unique('admin_user', ['username' => $user_name]);
            if ($email_count > 0) {
                $this->session->set_flashdata('error-msg', 'This email already used');
                redirect('admin/user_list');
            } elseif ($user_name_count > 0) {
                $this->session->set_flashdata('error-msg', 'This user name already used');
                redirect('admin/user_list');
            } else {
                $this->m_admin->create_user();
                $this->session->set_flashdata('success-msg', 'User Add Successfully !');
                redirect('admin/user_list');
            }
        }

        $roles = $this->m_admin->get_user_roles();
        $data['roles'] = $roles;
        $data['title'] = 'Smart Lock Admin';
        $data['menu'] = 'um';
        $data['main_content'] = 'admin/user/add_new';
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function user_list() {
        $data['user_list'] = $this->m_admin->user_list();
        $data['title'] = 'Smart Lock Admin';
        $data['menu'] = 'um';
        $data['main_content'] = 'admin/user/user_list';
        $this->load->view(THEMES_ADMIN, $data);
    }

    public function user_delete($userID = 0) {
        $user = $this->m_admin->get_table_row_with_id('admin_user', $userID);
        if (!empty($user)) {
            $role = (int) $user->role;
            if ($role > 1) {
                $this->m_admin->delete_table_row('admin_user', array('id' => $userID));
                $this->session->set_flashdata('success-msg', 'User successfully deleted');
            }
        }
        redirect('admin/user_list');
    }

}
