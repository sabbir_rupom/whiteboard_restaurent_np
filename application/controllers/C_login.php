<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include_once 'B_controller.php';

/**
 * @property M_admin $m_admin
 */
class C_login extends B_controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_admin');
    }

    public function index() {
        if ($this->session->has_userdata('username')) {
            redirect('admin');
        }
        $this->login();
    }

    public function login() {
        if ($this->input->post('username') && $this->input->post('password')) {
            $user = trim($this->input->post('username'));
            $pass = trim($this->input->post('password'));
            $verify = FALSE;
            if (strlen($pass) > 1) {
                $verify = $this->_verify_admin_user($user, $pass);
            }
        }


        $this->session->set_userdata('question_amount', 5);
        $this->session->set_userdata('question_timeout', 15);
        $data['title'] = 'Smart Lock |  Login';
        $this->load->view('login', $data);
    }

    private function _verify_admin_user($user, $pass) {
        $auth_check = $this->m_admin->verify_admin_credential($user, md5($pass));
        if ($auth_check) {
            redirect('admin');
        } else {
            redirect('login');
        }
    }
    
    public function verify_passcode() {
        $auth_check = FALSE;
        if ($this->input->post('passcode')) {
            $passcode = trim($this->input->post('passcode'));
            $auth_check = $this->m_admin->verify_passcode($passcode);
        }
        if ($auth_check) {
            $user_data = array(
                'question_amount' => 5,
                'question_timeout' => 15,
                'user' => TRUE
            );
            $this->session->set_userdata($user_data);
            echo 'success';
        } else {
            echo 'error';
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
    
    public function console() {
        $api = array(
            0 => array(
                'name' => 'get-user',
                'title' => 'Get User',
                'post' => 'deviceudid',
                'user_required' => FALSE
            ),
            1 => array(
                'name' => 'get-categories',
                'title' => 'Get Categories',
                'post' => '',
                'user_required' => TRUE
            ),
            2 => array(
                'name' => 'get-category-questions',
                'title' => 'Get Category Questions',
                'post' => 'categoryid,userid',
                'user_required' => TRUE
            ),
            3 => array(
                'name' => 'save-restaurant',
                'title' => 'Save Restaurant',
                'post' => 'restaurantid,name,email',
                'user_required' => TRUE
            ),
            4 => array(
                'name' => 'save-menu',
                'title' => 'Save Restaurant Menu',
                'post' => 'menuid,restaurantid,name,description,price',
                'user_required' => TRUE
            ),
            5 => array(
                'name' => 'get-restaurant',
                'title' => 'Get Restaurant',
                'post' => 'restaurantid',
                'user_required' => TRUE
            ),
            6 => array(
                'name' => 'get-menu',
                'title' => 'Get Menu',
                'post' => 'restaurantid',
                'user_required' => TRUE
            ),
            7 => array(
                'name' => 'save-order',
                'title' => 'Save order',
                'post' => 'restaurantid,menuid,unit',
                'user_required' => TRUE
            ),
            7 => array(
                'name' => 'get-order',
                'title' => 'Get user order',
                'post' => '',
                'user_required' => TRUE
            )
        );
        $data = array(
            'api' => $api,
            'title' => 'Console | Restaurant Whiteboard'
        );
        $this->load->view('console', $data);
    }

}
