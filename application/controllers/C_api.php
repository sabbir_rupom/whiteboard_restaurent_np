<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property M_app $m_app
 */
class C_api extends MY_Controller {

    public $is_hookable = TRUE;

    function __construct() {
//        echo 1; exit;
        parent::__construct();
        $this->load->model('m_app');
        $this->load->helper('jwt_token');
    }

    public function index() {
        throw new Api_Exception(Result_code::ACCESS_FORBIDDEN, 'Forbidden');
    }

    public function get_category_questions() {
        $this->loginRequired = TRUE;

        $this->_filter();
        
        $data = array();
        $category_id = $this->getValueFromJSON('categoryid', 'int', TRUE);
        $user_id = $this->getValueFromJSON('userid', 'int', TRUE);

        if ($user_id != $this->userId) {
            throw new Api_Exception(Result_code::USER_NOT_FOUND, 'User mismatched');
        }

        $userInfo = $this->m_app->get_table_row_with_id('app_users', $user_id);
        if (empty($userInfo)) {
            throw new Api_Exception(Result_code::USER_NOT_FOUND, 'User not found');
        }

        list($questions, $qustion_images) = $this->m_app->get_questions_by_category($category_id);

        if (empty($questions)) {
            throw new Api_Exception(Result_code::DATA_NOT_FOUND, 'Questions not found in Database');
        }

        /*
         * Zip create process begins
         */
        $zipname = 'quiz_' . $user_id . '_' . $category_id . '.zip'; // add a user specific zip name
        $filepath = LOCAL_QIMAGE_ZIP_PATH . 'quiz_' . $user_id . '_' . $category_id . '.zip';
        // If the user file already exists, delete it
        if (file_exists($filepath)) {
            unlink($filepath);
        }
        $zip = new ZipArchive;
        $zip->open($filepath, ZipArchive::CREATE);
        $qcnt = 1;
        foreach ($qustion_images as $key => $value) {
            $dot_pos = strripos($value, ".");
            $file_type = substr($value, $dot_pos); // getting the file extension

            $zip->addFile(LOCAL_QIMAGE_UPLOAD_PATH . $value, $key . $file_type); // add zip file by renaming the image name
            $qcnt++;
        }
        $zip->setPassword($userInfo->deviceudid);
        $zip->close();
        /*
         * Zip create process end
         */

        $data = array(
            'user_id' => $this->userId,
            'questions' => $questions,
            'zip_url' => base_url('download/' . $zipname),
            'success' => [
                'message' => "Question retrive successful",
                'title' => "Question Acquired"
            ]
        );
        Response::$result = $data;
    }

    public function get_categories() {
        $this->loginRequired = TRUE;

        $this->_filter();

        $categories = $data = array();
        $categories = $this->m_app->get_question_categories();
        if (empty($categories)) {
            throw new Api_Exception(Result_code::DATA_NOT_FOUND, 'Categories not found in Database');
        }
        $data = array(
            'user_id' => $this->userId,
            'categories' => $categories,
            'success' => [
                'message' => "Successfully Logged In",
                'title' => "Login Success"
            ]
        );
        Response::$result = $data;
    }
    
    public function save_restaurant() {
        $this->loginRequired = TRUE;

        $this->_filter();
        
        $restaurant_id = $this->getValueFromJSON('restaurantid', 'int', FALSE);
        $restaurant_name = $this->getValueFromJSON('name', 'string', TRUE);
        $email = $this->getValueFromJSON('email', 'string', TRUE);

        
        $restaurant = $this->m_app->save_restaurant($restaurant_id,$restaurant_name,$email);
        
        
        $data = array(
            'user_id' => $this->userId,
            'restaurant' => $restaurant,
            'success' => [
                'message' => "Restaurant created",
                'title' => "Success"
            ]
        );
        Response::$result = $data;
    }
    
    public function cast_query_results($rs) {
    $fields = mysqli_fetch_fields($rs);
    $data = array();
    $types = array();
    foreach($fields as $field) {
        switch($field->type) {
            case 3:
                $types[$field->name] = 'int';
                break;
            case 4:
                $types[$field->name] = 'float';
                break;
            default:
                $types[$field->name] = 'string';
                break;
        }
    }
    while($row=mysqli_fetch_assoc($rs)) array_push($data,$row);
    for($i=0;$i<count($data);$i++) {
        foreach($types as $name => $type) {
            settype($data[$i][$name], $type);
        }
    }
    return $data;
}
    
    public function get_restaurant() {
        $this->loginRequired = TRUE;

        $this->_filter();
        
        $restaurant_id = $this->getValueFromJSON('restaurantid', 'int', TRUE);

        
        $restaurant = $result = $this->m_app->get_table_row('restaurant', ["id"=>$restaurant_id]);
        
        
        $data = array(
            'user_id' => $this->userId,
            'restaurant' => $restaurant,
            'success' => [
                'message' => "Restaurant info",
                'title' => "Success"
            ]
        );
        Response::$result = $data;
    }
    
    public function get_menu() {
        $this->loginRequired = TRUE;

        $this->_filter();
        
        $restaurant_id = $this->getValueFromJSON('restaurantid', 'int', TRUE);
        
        $menu = $result = $this->m_app->get_table_data('menu', ["restaurant_id"=>$restaurant_id]);
        
        
        $data = array(
            'user_id' => $this->userId,
            'menu' => $menu,
            'success' => [
                'message' => "Menu info",
                'title' => "Success"
            ]
        );
        Response::$result = $data;
    }
    
    public function get_order() {
        $this->loginRequired = TRUE;

        $this->_filter();
        
        $token_id = $this->m_app->get_table_row('authsessions', ["userid"=>$this->userId]);
        $orders = $this->m_app->get_table_data('orders', ["user_id"=>$this->userId,"tokenid"=>$token_id->tokenid]);
        
        
        
        $data = array(
            'user_id' => $this->userId,
            'orders' => $orders,
            'success' => [
                'message' => "Order info",
                'title' => "Success"
            ]
        );
        Response::$result = $data;
    }
    
    public function save_menu() {
        $this->loginRequired = TRUE;

        $this->_filter();
        
        $menu_id = $this->getValueFromJSON('menuid', 'int', FALSE);
        $restaurant_id = $this->getValueFromJSON('restaurantid', 'int', TRUE);
        $restaurant_menu = $this->getValueFromJSON('name', 'string', TRUE);
        $menu_desc = $this->getValueFromJSON('description', 'string', TRUE);
        $email = $this->getValueFromJSON('price', 'int', TRUE);

        
        $menu = $this->m_app->save_menu($this->userId,$menu_id,$restaurant_id,$restaurant_menu,$menu_desc,$email);
        
        
        $data = array(
            'user_id' => $this->userId,
            'menu' => $menu,
            'success' => [
                'message' => "Restaurant menu created",
                'title' => "Success"
            ]
        );
        Response::$result = $data;
    }
    
    public function save_order() {
        $this->loginRequired = TRUE;

        $this->_filter();
        
        $menu_id = $this->getValueFromJSON('menuid', 'int', FALSE);
        $restaurant_id = $this->getValueFromJSON('restaurantid', 'int', TRUE);
        $unit = $this->getValueFromJSON('unit', 'int', TRUE);

        
        $menu = $this->m_app->save_order($this->userId,$menu_id,$restaurant_id,$unit);
        
        
        $data = array(
            'user_id' => $this->userId,
            'menu' => $menu,
            'success' => [
                'message' => "Order created",
                'title' => "Success"
            ]
        );
        Response::$result = $data;
    }

    public function get_user() {
        $this->_filter();

        $data = array();
        $user_id = 0;

        $device_uuid = $this->getValueFromJSON('deviceudid', 'string', TRUE);
        $where = array(
            'deviceudid' => $device_uuid
        );
        $result = $this->m_app->get_table_row('app_users', $where);
        if (!empty($result)) {
            $user_id = (int) $result->id;
        } else {
            $insert_user = [
                'deviceudid' => $device_uuid,
                'status' => 1
            ];
            $user_id = $this->m_app->insert_into_table('app_users', $insert_user);
        }
        if ($user_id) {
            $sessionToken = $this->m_app->auth_session_update($user_id);
            
            $data = array(
                'user_id' => $user_id,
                'sessionToken' => $sessionToken,
                'success' => [
                    'message' => "Successfully Logged In",
                    'title' => "Login Success"
                ]
            );
        } else {
            throw new Api_Exception(Result_code::USER_NOT_FOUND, 'Database Error Occured');
        }

        Response::$result = $data;
    }

    public function download($file_name = '') {
        $this->_filter();
        
        if (empty($file_name)) {
            throw new Api_Exception(Result_code::NOT_FOUND_404, 'File not found');
        }
        $dot_pos = strripos($file_name, ".");
        $file_type = substr($file_name, $dot_pos); // getting the file extension
        if (strpos($file_type, 'zip') !== false) {
            $zipFile = LOCAL_QIMAGE_ZIP_PATH . $file_name;

            if (file_exists($zipFile)) {
                $file_name = basename($zipFile);

                header("Content-Type: application/zip");
                header("Content-Disposition: attachment; filename=$file_name");
                header("Content-Length: " . filesize($zipFile));

                readfile($zipFile);
                exit;
            }

            throw new Api_Exception(Result_code::NOT_FOUND_404, 'File not found');
        }
    }

}
