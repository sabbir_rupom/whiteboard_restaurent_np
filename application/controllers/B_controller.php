<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class B_controller extends CI_Controller {
    
    protected $header;
    protected $json;

    protected $admin = FALSE;
    protected $_limit = 15;
    protected $sort_type = 'ASC';
    protected $sort_column = 'id';
    protected $_sorting = array(
        'column' => 'id',
        'type' => 'ASC',
        'limit' => 15
    );
    protected $requestMethod;

    function __construct() {
        parent::__construct();
        $this->requestMethod = $this->input->method(TRUE);
    }

    protected function jsonRemoveUnicodeSequences($struct) {
        $result = @preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
        $this->output->set_content_type('application/json')
                ->set_output($result);
        $this->output->_display();
        exit;
    }

    protected function _uploadFiles($uploadPath, $field, $imageName = '') {
        $config = array(
            'allowed_types' => 'jpg|gif|png|jpeg|JPG|PNG',
            'upload_path' => $uploadPath,
            'encrypt_name' => empty($imageName) ? TRUE : FALSE,
            'overwrite' => empty($imageName) ? FALSE : TRUE,
            'max_size' => 3072
        );
        if (!empty($imageName)) {
            $config['file_name'] = $imageName;
        }
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload($field)) {
            $image_data = $this->upload->data();
            return ['file_name' => $image_data['file_name']];
        } else {
            $msg['error-msg'] = $this->upload->display_errors();
            $this->session->set_flashdata($msg);
            redirect('admin/question_new');
        }
    }

}
